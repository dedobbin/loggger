#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <sstream>

// Forward declaration
template <typename... Args>
inline void args_to_vector(std::vector<std::string>& output, Args... args);

// For non-specialized types, convert to string using stringstream
template <typename T>
inline std::string to_string_specialized(const T& value) {
    std::stringstream ss;
    ss << value;
    return ss.str();
}

// Specialization for std::string
template <>
inline std::string to_string_specialized<std::string>(const std::string& value) {
    return value;
}

// Specialization for bool 
template <>
inline std::string to_string_specialized<bool>(const bool& value) {
    return value ? "true" : "false";
}

// Base case for when there are no more arguments to process
inline void args_to_vector(std::vector<std::string>& output) {
   // nothing
}

template <typename First, typename... Rest>
inline void args_to_vector(std::vector<std::string>& output, First first, Rest... rest) {
    output.push_back(to_string_specialized(first)); 
    args_to_vector(output, rest...);
}

inline std::string interpolate_inner(const std::vector<std::string> &input){
    // TODO: check if {}'s equal amount of arguments
    std::string tmpl = input[0];
    for (unsigned int i = 1; i < input.size(); i++){
        std::string::size_type pos = tmpl.find("{}");
        if (pos == std::string::npos){
            break;
        }
        tmpl.replace(pos, 2, input[i]);
    }
    return tmpl;
}

template <typename... Args>
inline std::string interpolate(Args... args){
    std::vector<std::string> segments; 
    args_to_vector(segments, args...); 
    return interpolate_inner(segments);
}

#ifndef LOGGGER_LEVEL
    #define LOGGGER_LEVEL 4
#endif

#if LOGGGER_LEVEL > 5
    #define LOGGGER_TRACE(...) std::cout << "TRACE: " << interpolate(__VA_ARGS__) << std::endl
#else
    #define LOGGGER_TRACE(...) 
#endif

#if LOGGGER_LEVEL > 4
    #define LOGGGER_DEBUG(...) std::cout << "DEBUG: " << interpolate(__VA_ARGS__) << std::endl
#else
    #define LOGGGER_DEBUG(...) 
#endif
    
#if LOGGGER_LEVEL > 3
    #define LOGGGER_INFO(...) std::cout << "INFO: " << interpolate(__VA_ARGS__) << std::endl
#else
    #define LOGGGER_INFO(...) 
#endif

#if LOGGGER_LEVEL > 2
    #define LOGGGER_WARNING(...) std::cout << "WARNING: " << interpolate(__VA_ARGS__) << std::endl
#else
    #define LOGGGER_WARNING(...) 
#endif

#if LOGGGER_LEVEL > 1
    #define LOGGGER_ERROR(...) std::cout << "ERROR: " << interpolate(__VA_ARGS__) << std::endl
#else
    #define LOGGGER_ERROR(...) 
#endif

#if LOGGGER_LEVEL > 0
    #define LOGGGER_CRITICAL(...) std::cout << "CRITICAL: " << interpolate(__VA_ARGS__) << std::endl
#else
    #define LOGGGER_CRITICAL(...) 
#endif